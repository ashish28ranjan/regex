Regex:

^(?!(https:\/\/|http:\/\/|www\.|mailto:|smtp:|ftp:\/\/|ftps:\/\/))(((([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,86}[a-zA-Z0-9]))\.(([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,73}[a-zA-Z0-9]))\.(([a-zA-Z0-9]{2,12}\.[a-zA-Z0-9]{2,12})|([a-zA-Z0-9]{2,25})))|((([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,162}[a-zA-Z0-9]))\.(([a-zA-Z0-9]{2,12}\.[a-zA-Z0-9]{2,12})|([a-zA-Z0-9]{2,25}))))$

Description:
This regex validates domains.

PASS:
fb.com
facebook.com
fb.co.uk
facebook.co.uk
developer.fb.com
developer.fb.co.in
dev.admin.facebook.com
dev-admin.facebook.co.uk
dev-admin.dev-facebook.co.uk
dev-admin.dev-facebook-england.co.uk
dev-admin.dev-facebook-england.com


FAIL:
dev.admin.facebook.co.in
dev.master.admin.fb.com
dev.master.admin.fb.co.uk
http://fb.com
https://fb.com
www.fb.com
mailto:fb.com
smtp:fb.com
ftp://fb.com
ftps://fb.com
